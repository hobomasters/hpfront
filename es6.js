/* eslint-disable no-undef,no-unreachable */
import * as asn1js from "asn1js";
import * as axios from "axios";
import { arrayBufferToString, stringToArrayBuffer, toBase64, fromBase64 } from "pvutils";
import { getCrypto, getAlgorithmParameters, setEngine } from "../../src/common.js";
import { formatPEM } from "../../examples/examples_common.js";
import CertificationRequest from "../../src/CertificationRequest.js";
import AttributeTypeAndValue from "../../src/AttributeTypeAndValue.js";
import Attribute from "../../src/Attribute.js";
import Extension from "../../src/Extension.js";
import Extensions from "../../src/Extensions.js";
import RSAPublicKey from "../../src/RSAPublicKey.js";
import GeneralNames from "../../src/GeneralNames.js";
import GeneralName from "../../src/GeneralName.js";

let pkcs10Buffer = new ArrayBuffer(0);

let hashAlg = "SHA-1";
let signAlg = "RSASSA-PKCS1-V1_5";

function createPKCS10Internal()
{
	let sequence = Promise.resolve();
	
	const pkcs10 = new CertificationRequest();
	
	let publicKey;
	let privateKey;
	
	const crypto = getCrypto();
	if(typeof crypto === "undefined")
		return Promise.reject("No WebCrypto extension found");
	

	let keySize = parseInt(document.getElementById("key_size").value);
	let dn_c = document.getElementById("dn_c").value;
	let dn_cn = document.getElementById("dn_cn").value;
	let dn_email = document.getElementById("dn_email").value;
	let dn_ou = document.getElementById("dn_ou").value;
	let dn_o = document.getElementById("dn_o").value;
	let dn_l = document.getElementById("dn_l").value;

	pkcs10.version = 0;
	pkcs10.subject.typesAndValues.push(new AttributeTypeAndValue({
		type: "2.5.4.6",
		value: new asn1js.PrintableString({ value: dn_c })
	}));
	pkcs10.subject.typesAndValues.push(new AttributeTypeAndValue({
		type: "2.5.4.3",
		value: new asn1js.Utf8String({ value: dn_cn })
	}));
	pkcs10.subject.typesAndValues.push(new AttributeTypeAndValue({
		type: "1.2.840.113549.1.9.1",
		value: new asn1js.Utf8String({ value: dn_email })
	}));
	pkcs10.subject.typesAndValues.push(new AttributeTypeAndValue({
		type: "2.5.4.11",
		value: new asn1js.Utf8String({ value: dn_ou })
	}));
	pkcs10.subject.typesAndValues.push(new AttributeTypeAndValue({
		type: "2.5.4.10",
		value: new asn1js.Utf8String({ value: dn_o })
	}));
	pkcs10.subject.typesAndValues.push(new AttributeTypeAndValue({
		type: "2.5.4.7",
		value: new asn1js.Utf8String({ value: dn_l })
	}));
	
	pkcs10.attributes = [];
	
	sequence = sequence.then(() =>
	{
		const algorithm = getAlgorithmParameters(signAlg, "generatekey");
		if("modulusLength" in algorithm.algorithm)
			algorithm.algorithm.modulusLength = keySize;

		if("hash" in algorithm.algorithm)
			algorithm.algorithm.hash.name = hashAlg;
		
		return crypto.generateKey(algorithm.algorithm, true, algorithm.usages);
	});
	
	sequence = sequence.then(keyPair =>
	{
		publicKey = keyPair.publicKey;
		privateKey = keyPair.privateKey;
	},
	
	error => Promise.reject((`Error during key generation: ${error}`))
	);
	
	sequence = sequence.then(() => pkcs10.subjectPublicKeyInfo.importKey(publicKey));

	sequence = sequence.then(() => crypto.digest({ name: "SHA-1" }, pkcs10.subjectPublicKeyInfo.subjectPublicKey.valueBlock.valueHex))
		.then(result =>
		{
			pkcs10.attributes.push(new Attribute({
				type: "1.2.840.113549.1.9.14",
				values: [(new Extensions({
					extensions: [
						new Extension({
							extnID: "2.5.29.14",
							critical: false,
							extnValue: (new asn1js.OctetString({ valueHex: result })).toBER(false)
						}),
						new Extension({
							extnID: "1.2.840.113549.1.9.7",
							critical: false,
							extnValue: (new asn1js.PrintableString({ value: "passwordChallenge" })).toBER(false)
						})
					]
				})).toSchema()]
			}));
		}
		);

	sequence = sequence.then(() => pkcs10.sign(privateKey, hashAlg), error => Promise.reject(`Error during exporting public key: ${error}`));
	
	return sequence.then(() =>
	{
		pkcs10Buffer = pkcs10.toSchema().toBER(false);
	}, error => Promise.reject(`Error signing PKCS#10: ${error}`));
}

function savePfx() {
	
}

function sendToAuthority() {
	let responseContent
	const instance = axios.create({
		baseURL: 'https://some-domain.com/api/',
		timeout: 10000,
		headers: {'X-Custom-Header': 'foobar'}
	  });
	
	  instance.post('/user', {
		firstName: 'Fred',
		lastName: 'Flintstone'
	  })
	  .then(function (response) {
		console.log(response);
	  })
	  .catch(function (error) {
		console.log(error);
	  });
}

function createPKCS10()
{
	return Promise.resolve().then(() => createPKCS10Internal()).then(() =>
	{
		let resultString = "-----BEGIN NEW CERTIFICATE REQUEST-----\r\n";
		resultString = `${resultString}${formatPEM(toBase64(arrayBufferToString(pkcs10Buffer)), 76)}`;
		resultString = `${resultString}\r\n-----END NEW CERTIFICATE REQUEST-----\r\n`;
		
		document.getElementById("pem-text-block").value = resultString;
	});
}


context("Hack for Rollup.js", () =>
{
	return;
	
	createPKCS10();
	setEngine();
});