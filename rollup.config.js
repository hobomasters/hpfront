import rollupNodeResolve from "rollup-plugin-node-resolve";

export default {
	input: "es6.js",
	plugins: [
		rollupNodeResolve({ jsnext: true, main: true })
	],
	output: [
		{
			file: "bundle.js",
			format: "iife",
			outro: `
window.createPKCS10 = createPKCS10;

function context(name, func) {}`
		},
		{
			file: "../../test/browser/pkcs10ComplexExample.js",
			format: "es"
		}
	]
};